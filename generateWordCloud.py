import matplotlib.pyplot as plt
from wordcloud import WordCloud, STOPWORDS
import operator
from scipy.misc import imread


def generate(result,showResult = False,debug = False):

    if debug:
        print "WORDCLOUD: Started."

    numOfClouds = len(result.keys())
    i = 0
    for articleName in result.keys():
        if debug:
            print "WORDCLOUD: "+str(i)+"/"+str(numOfClouds)
            i = i + 1
        article = result[articleName]
        articleSorted = sorted(article.items(), key=operator.itemgetter(1))
        articleSorted = articleSorted[0:150]
        values = []
        for i in range(len(articleSorted)):
            values.append(articleSorted[i][1])
        minValue = min(values)
        maxValue = max(values)
        diff = (maxValue - minValue)
        if diff == 0:
            diff = 1/1000000.0
        values = None
        for i in range(len(articleSorted)):
            tmp = (articleSorted[i][1] - minValue) / diff
            articleSorted[i] = (articleSorted[i][0],tmp * 1000000)

        wiki_mask = imread('wikipedia_mask.png', flatten=True)
        wordcloud = WordCloud(font_path='Helvetica.ttf',width= 1000, height = 1000 ,background_color='white',mask=wiki_mask)
        wordcloud.generate_from_frequencies(articleSorted)
        plt.imshow(wordcloud)
        plt.axis('off')
        plt.savefig('wordclouds/'+articleName+".png", dpi=300)
        if showResult:
            plt.show()
