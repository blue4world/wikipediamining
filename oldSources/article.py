# -*- coding: utf-8 -*-
from collections import Counter
import gc
import re

class Article:
    def __init__(self,id,title,saveOriginalText = False):
        self.title = title.replace("\n",'')
        self.id = int(id.replace("\n",''))
        self.wordsTF = {}
        self.inputText = ""

    def isProcessed(self):
        if len(self.wordsTF.keys()) > 0:
            return  True
        else:
            return  False

    def setText(self,inputText):
        self.inputText = inputText

    def calculate(self,preprocess = False):
        words = self.__prepareWords(self.inputText,preprocess)
        self.inputText = ""
        gc.collect()
        self.wordsTF = self.__calculateTF(words)

    def getTitle(self):
        return self.title

    def getWordsTF(self):
        return self.wordsTF

    def getAllWords(self):
        return self.wordsTF.keys()

    def isWordInArticle(self,word):
        if self.wordsTF[word] != None:
            return True
        return False

    def __calculateTF(self,words):
        wordsLen = len(words)
        counter = Counter(words)
        wordsTF = {}
        for word in words:
            wordsTF[word] = counter[word] / float(wordsLen)
        return wordsTF

    def __prepareWords(self,inputText,preprocess):
        words = inputText.replace("\n",' ')
        words = words.lower()
        if preprocess:
            # remove crap characters
            regex = re.compile('[\–\-,\.!?\"\(\)\„\:\=1234567890]')
            words = regex.sub('',words)
            # remove stop words
            stopList = {"a","v","je","se","na","s","z","jako","pro","byl","do","ve","i","od","jsou","o","jeho","k","za","po","ze"}
            remove = '|'.join(stopList)
            regex = re.compile(r'\b('+remove+r')\b', flags=re.IGNORECASE|re.UNICODE)
            words = regex.sub('',words)

        words = words.split(" ")
        words = filter(None, words)
        return  words

    def __str__(self):
        return str(self.title)+": "+str(self.wordsTF)
