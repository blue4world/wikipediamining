import math

from oldSources.article import *


class ArticleHolder:
    def __init__(self):
        self.articles = []
        self.idf = {}

    def getArticles(self):
        return  self.articles

    def isAllProcessed(self):
        i = 0
        for article in self.articles:
            if not article.isProcessed():
                return False
        i += 1
        return True

    def getArticlesByRange(self,rangeFrom,rangeTo):
        return self.articles[rangeFrom:rangeTo+1]

    def load(self,filename,debug = False):
        f = open(filename, 'r')
        text = ""
        readingText = False
        i = 0
        while 1:
            line = f.readline()
            if not line:
                break
            # end of article
            if line == "</doc>\n":
                readingText = False
                article.setText(text)
                self.articles.append(article)
                text = ""
            # reading content
            if readingText:
                text = text + line
            # start of article
            if line[0:4] == "<doc":
                # debug out start
                if debug:
                    i += 1
                    if i % 10 == 0:
                        print "Loading article: "+str(i)
                # debug out end
                attrs = line.split('"')
                article = Article(id=attrs[1],title=attrs[5])
                readingText = True
        if debug:
            print "Total article count: "+str(i)


    def calculate(self,preprocess = False,debug = False):
        self.__calculateTF(preprocess,debug)
        self.__calculateIDF(debug)


    def __calculateTF(self,preprocess,debug):
        i = 0
        for article in self.articles:
            # debug out
            if debug:
                i += 1
                if i % 10 == 0:
                    print "Calculating TF: "+str(i)
            article.calculate(preprocess=preprocess)


    def __calculateIDF(self,debug):
        idf = {}
        i = 0
        for a in self.articles:
            # debug out
            if debug:
                i += 1
                if i % 10 == 0:
                    print "Calculating IDF: "+str(i)

            for b in a.getAllWords():
               if idf.has_key(b):
                   idf[b] += 1
               else:
                   idf[b] = 1

        numOfArticles = len(self.articles)
        for a in idf.keys():
            idf[a] = math.log10(float(numOfArticles) / idf[a])
        self.idf = idf

    def splitToChunks(self,numOfchunks):
        arrayOfNumbers = []
        arrayOfOriginNumbers = []
        actualIndex = 0       
        averageNumber = int(math.floor(len(self.articles)/numOfchunks))
        maxRange = 0
        for a in range(numOfchunks):
            if a == numOfchunks - 1:
                arrayOfNumbers.append((actualIndex, (len(self.articles)) - 1))                
            else:
                maxRange = maxRange + averageNumber
                arrayOfOriginNumbers.append(maxRange)
                arrayOfNumbers.append((actualIndex, maxRange - 1))
                actualIndex = maxRange 

        return arrayOfNumbers


