from multiprocessing import Lock
import multiprocessing

from oldSources.articleHolder import *


filename = "wiki_medium.xml"
holder = ArticleHolder()
holder.load(filename,debug=True)
articles = holder.getArticles();


def worker(articleRange, holder):
    mutex.acquire()
    articles = holder.getArticlesByRange(articleRange[0],articleRange[1])
    mutex.release()
    i = 0
    for article in articles:
        article.calculate(preprocess=False)
        i = i + 1


threads = []
mutex = Lock()
cpu_count = multiprocessing.cpu_count()
numOfChunks = holder.splitToChunks(cpu_count)
print numOfChunks
# Start
for i in range(cpu_count):
    t = multiprocessing.Process(target=worker, args=(numOfChunks[i],holder))
    t.start()
    threads.append(t)

# Join
for thread in threads:
    thread.join()

print holder.isAllProcessed()