#!/usr/bin/env python
# -*- coding: utf-8 -*-

from articleHolder import ArticleHolder
import generateWordCloud
import multiprocessing
from multiprocessing import Manager
import math


# get cpuCount by system or set it force by
#cpuCount = 1
cpuCount = multiprocessing.cpu_count()


# load from file
holder = ArticleHolder(cpuCount)
holder.loadFromFile("wiki_small.xml",debug=True)

# TF
holder.calculateTF(preprocess=True,debug=True)
#holder.saveTFtoFile(debug=True)
# IDF
holder.calculateIDF(debug=True)
#holder.saveIDFtoFile(debug=True)
# TF-IDF
holder.calculateTF_IDF(debug=True)
#holder.saveTF_IDFtoFile(debug=True)

generateWordCloud.generate(holder.getReusults(),debug=True)





