import codecs
import operator

class Save:
    @staticmethod
    def saveTF_IDFtoFile(articlesTF,filename = "TF-IDF.txt",debug = False):
        Save.saveTFtoFile(articlesTF,filename,debug)

    @staticmethod
    def saveTFtoFile(articlesTF,filename = "TF.txt",debug = False):
        if debug: print "SAVE: Saving to: "+filename
        file = codecs.open(filename,'w','utf-8')
        for key in articlesTF.keys():
            dic = articlesTF[key]
            dicSorted = sorted(dic.items(), key=operator.itemgetter(1))
            file.write('<article name="'+key+'">\n')
            for i in range(len(dicSorted)):
                    item = dicSorted[i]
                    file.write(item[0])
                    file.write(":")
                    file.write(str(item[1]))
                    file.write("\n")
            file.write("</article>\n")
        file.close()
        if debug: print "SAVE: Saving complete"

    @staticmethod
    def saveIDFtoFile(idf,filename = "IDF.txt",debug = False):
        if debug: print "SAVE: Saving to: "+filename
        file = codecs.open(filename,'w','utf-8')
        idfSorted = sorted(idf.items(), key=operator.itemgetter(1))
        for i in range(len(idfSorted)):
            item = idfSorted[i]
            file.write(item[0])
            file.write(":")
            file.write(str(item[1]))
            file.write("\n")
        file.close()
        if debug: print "SAVE: Saving complete"
