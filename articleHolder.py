# -*- coding: utf-8 -*-

from multiprocessing import Manager
import multiprocessing
try:
    from collections import Counter
except ImportError:
    from counter import Counter
import math
import gc
from saveToFile import Save
import re
import unicodedata
import codecs

class ArticleHolder:

    def __init__(self,threadsCount):
        self.inputArticles = []
        self.threadsCount = threadsCount
        self.idf = {}
        self.articlesTF = []
        self.manager = Manager()
        self.inputArticles = self.manager.dict()

    #### TF
    def __workerTF(self,inputDic,sharedDic,manager,id,debug,preprocess):
        if debug: print "TF: Starintg thread: "+str(id)
        i = 0
        for key in inputDic.keys():
            words = inputDic[key]
            words = words.replace("\n",' ')
            words = words.lower()
            if preprocess:
                # remove crap characters
                regex = re.compile('[\–\-\,\.\*\!\?\\"\(\)\„\:\=1234567890]')
                words = regex.sub('',words)
                # remove stop words
                stopList = ["a","v","je","se","na","s","z","jako","pro","byl","do","ve","i","od","jsou","o","jeho","k","za","po","ze","až","ho"]
                #stopList = ["a","u","i","v","z","k","že","se","než","tímto","budeš","budem","byli","jseš","můj","svým","tomto","tohle","tuto","tyto","jej","zda","proč","máte","tato","kam","tohoto","kteří","mi","nám","tomu","tomuto","mít","nic","proto","kterou","byla","toho","protože","asi","ho","naši","tím","takže","svých","její","svými","jist","tu","této","bylo","kde","ke","právě","ji","nad","nejsou","či","pod","mezi","přes","ty","pak","vám","ani","když","jsem","aby","jsme","před","jejich","byl","ještě","až","bez","také","pouze","první","vaše","která","nás","nový","pokud","může","jeho","jiné","nové","není","vás","jen","podle","zde","už","být","více","bude","již","než","by","které","nebo","ten","má","při","od","po","jsou","další","ale","si","ve","jako","za","zpět","ze","do","pro","je","na","s","o","skrz","kromě","mimo","díky","kvůli","oproti","prostřednictvím","stran","pomocí","během","vedle","okolo","podél","kolem","místo","já","on","ono","my","vy","oni","ony","ona","sebe","můj","tvůj","jeho","její","náš","váš","jejich","svůj","ta","to","ti","tento","tenhle","onen","takový","týž","tentýž","sám","kdo","co","který","jaký","čí","jenž","ať","nechť","kéž","ano","ne","což","ovšem","zajisté","jak","tak","hned","jednak","zčásti","avšak","však","leč","naopak","jenže","sice","ba","dokonce","nejen","nýbrž","totiž","vždyť","neboť","tudíž","tedy","holt","bychom","byste","jsi","jenom","ode","prostě","velice"]
                remove = '|'.join(stopList)
                regex = re.compile(r'\b('+remove+r')\b', flags=re.IGNORECASE)
                words = regex.sub('',words)

            words = words.split(" ")
            words = filter(None, words)
            wordsLen = len(words)
            counter = Counter(words)
            dic = manager.dict()
            for word in words:
                dic[word] = counter[word] / float(wordsLen)
            sharedDic[key] = dic
            i = i + 1
        if debug: print "TF: End of thread "+str(id)


    def calculateTF(self,dropInputData = True,preprocess=True,debug = False):
        if debug: print "TF: Started"

        chunksRange = self.__splitToChunks(len(self.articlesTF),self.threadsCount,returnValue="intervals")
        sharedDic = self.manager.dict()

        # RUN
        if debug: print "TF: Starting Threads"
        threads = []
        for i in range(0,self.threadsCount):
            t = multiprocessing.Process(target=self.__workerTF, args=(self.inputArticles,sharedDic,self.manager,i,debug,preprocess))
            t.start()
            threads.append(t)

        # JOIN
        for t in threads:
            t.join()


        if dropInputData:
            articlesInput = []
            gc.collect()

        self.articlesTF = sharedDic

    ### TF_IDF
    def __workerIDF(self,sharedDic,rangeFrom,rangeTo,articlesTF,id,debug):
            if debug: print "IDF: Starintg thread: "+str(id)
            if debug: print "IDF: ID: "+str(id)+" from : "+str(rangeFrom)+" to: "+str(rangeTo)

            keys = articlesTF.keys()
            keys = keys[rangeFrom:rangeTo+1]
            numOfArticles = len(articlesTF)
            i = 0
            for key in keys:
                if debug:
                    if (rangeTo-i) % 1000 == 0:
                        print "IDF: ID: "+str(id)+" left: "+str(rangeTo-i)
                a = articlesTF[key]
                for b in a.keys():
                   if sharedDic.has_key(b):
                       sharedDic[b] += 1
                   else:
                       sharedDic[b] = 1
            if debug: print "IDF: ID: "+str(id)+" End of thread."

    def __workerIDF_Log10(self,sharedDic,entryCount,rangeFrom,rangeTo,id,debug):
        if debug: print "IDF-LOG: Starintg thread: "+str(id)
        if debug: print "IDF-LOG: ID: "+str(id)+" from : "+str(rangeFrom)+" to: "+str(rangeTo)
        keys = sharedDic.keys()
        keys = keys[rangeFrom:rangeTo+1]
        for key in keys:
            sharedDic[key] = math.log10(float(entryCount) / sharedDic[key])
        if debug: print "IDF-LOG: End of thread "+str(id)


    def calculateIDF(self,debug = False):
        if debug: print "IDF: Started"
        chunksRange = self.__splitToChunks(len(self.articlesTF.keys()),self.threadsCount,returnValue="intervals")

        manager = Manager()
        sharedDic = manager.dict()

        if debug: print "IDF: Starting Threads"
        threads = []
        for i in range(0,self.threadsCount):
            t = multiprocessing.Process(target=self.__workerIDF, args=(sharedDic,chunksRange[i][0],chunksRange[i][1],self.articlesTF,i,debug))
            t.start()
            threads.append(t)

        for t in threads:
            t.join()

        # calculate log
        entryCount = len(sharedDic.keys())
        chunksRange = self.__splitToChunks(entryCount,self.threadsCount,returnValue="intervals")
        if debug: print "IDF-LOG: Starting Threads"
        for i in range(0,self.threadsCount):
            t = multiprocessing.Process(target=self.__workerIDF_Log10, args=(sharedDic,entryCount,chunksRange[i][0],chunksRange[i][1],i,debug))
            t.start()
            threads.append(t)

        for t in threads:
            t.join()

        self.idf = sharedDic

    ### TF-IDF
    def __workerTF_IDF(self,articles,idf,rangeFrom,rangeTo,id,debug):
                if debug: print "TF-IDF: Starintg thread: "+str(id)
                if debug: print "TF-IDF: ID: "+str(id)+" from : "+str(rangeFrom)+" to: "+str(rangeTo)

                keys = articles.keys()
                keys = keys[rangeFrom:rangeTo+1]
                numOfArticles = len(articles)
                i = 0
                for key in keys:
                    dic = articles[key]
                    for word in dic.keys():
                        if idf.has_key(word):
                            dic[word] = dic[word] * idf[word]

                if debug: print "TF-IDF: ID: "+str(id)+" End of thread."

    def calculateTF_IDF(self, debug = True):
        if debug: print "TF-IDF: Started"
        chunksRange = self.__splitToChunks(len(self.articlesTF),self.threadsCount,returnValue="intervals")
        if debug: print "TF-IDF: Starting Threads"
        threads = []
        for i in range(0,self.threadsCount):
            t = multiprocessing.Process(target=self.__workerTF_IDF, args=(self.articlesTF,self.idf,chunksRange[i][0],chunksRange[i][1],i,debug))
            t.start()
            threads.append(t)

        for t in threads:
            t.join()


    def loadFromFile(self,filename,debug=False):
        #f = open(filename, 'r')
        f = codecs.open(filename,'r','utf-8')
        if debug: print "LOAD: Processing from: "+str(filename)
        text = ""
        title = ""
        readingText = False
        i = 0
        while 1:
            line = f.readline()
            if not line:
                break
            # end of article
            if line == "</doc>\n":
                readingText = False
                #self.inputArticles[title] = ArticleHolder.removeAccents(text.decode("utf-8"))
                #self.inputArticles[title] = text.decode("utf-8")
                self.inputArticles[title] = text
                text = ""
            # reading content
            if readingText:
                text = text + line
            # start of article
            if line[0:4] == "<doc":
                if debug and (i % 1000 == 0):
                    print "LOAD: Processing: "+str(i)
                i = i+1
                # debug out end
                attrs = line.split('"')
                title = attrs[5]
                readingText = True
        if debug: print "LOAD: Loaded "+str(i)+" articles to memory"

    @staticmethod
    def removeAccents(data):
        nkfd_form = unicodedata.normalize('NFKD', data)
        return u"".join([c for c in nkfd_form if not unicodedata.combining(c)])

    ### OTHERS
    @staticmethod
    def __splitToChunks(numOfArticles,numOfchunks,returnValue):
        arrayOfChunks = []
        arrayOfOriginNumbers = []
        actualIndex = 0
        averageNumber = int(math.floor(numOfArticles/float(numOfchunks)))
        maxRange = 0
        for a in range(numOfchunks):
            if a == numOfchunks - 1:
                arrayOfChunks.append((actualIndex, (numOfArticles - 1)))
            else:
                maxRange = maxRange + averageNumber
                arrayOfOriginNumbers.append(maxRange)
                arrayOfChunks.append((actualIndex, maxRange - 1))
                actualIndex = maxRange

        arrayOfOriginNumbers.append(numOfArticles)
        if returnValue == "borders":
            return arrayOfOriginNumbers
        elif returnValue == "intervals":
            return arrayOfChunks
        else:
            raise Exception('Invalid parameter returnValue')


    def saveTF_IDFtoFile(self,filename = "TF-IDF.txt",debug = False):
        Save.saveTF_IDFtoFile(self.articlesTF,filename,debug)

    def saveTFtoFile(self,filename = "TF.txt",debug = False):
        Save.saveTFtoFile(self.articlesTF,filename,debug)

    def saveIDFtoFile(self,filename = "IDF.txt",debug = False):
        Save.saveIDFtoFile(self.idf,filename,debug)

    def getReusults(self):
        return self.articlesTF